#!/bin/sh -e

VERSION=$2
TAR=../tiger-types_$VERSION.orig.tar.gz
DIR=tiger-types-$VERSION
TAG=$(echo "tiger-types-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export https://svn.java.net/svn/tiger-types~svn/tags/${TAG}/ $DIR 
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
