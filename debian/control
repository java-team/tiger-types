Source: tiger-types
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: James Page <james.page@canonical.com>
Build-Depends: cdbs, debhelper (>= 7), default-jdk, maven-debian-helper
Build-Depends-Indep:
 default-jdk-doc,
 junit,
 libmaven-compiler-plugin-java (>= 2.0.2),
 libmaven-javadoc-plugin-java (>= 2.6.1)
Standards-Version: 3.9.2
Homepage: http://java.net/projects/tiger-types
Vcs-Git: git://git.debian.org/git/pkg-java/tiger-types.git
Vcs-Browser: http://git.debian.org/?p=pkg-java/tiger-types.git

Package: libtiger-types-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: Type arithmetic library for Java5
 This library provides functions that perform type arithemtic over
 the type system of Java5. For example, you can compute that
 List<String> is a sub-type of Collection<String> but not
 Collection<Object>, you can compute the erasure of
 java.lang.reflect.Type, or you can determine the array component
 type T from A[T]

Package: libtiger-types-java-doc
Architecture: all
Section: doc
Depends: default-jdk-doc, ${maven:DocDepends}, ${misc:Depends}
Recommends: ${maven:DocOptionalDepends}
Suggests: libtiger-types-java
Description: Documentation for Type arithmetic library for Java5
 This library provides functions that perform type arithemtic over
 the type system of Java5. For example, you can compute that
 List<String> is a sub-type of Collection<String> but not
 Collection<Object>, you can compute the erasure of
 java.lang.reflect.Type, or you can determine the array component
 type T from A[T]
 .
 This package provides the API documentation for libtiger-types-java.
